<h1>Create new item</h1>
<#if success?? && success>
    <h2>Item added successfully!!</h2>
</#if>
<form action="/items/new" method="post">
    Title: <input type="text" name="title"><br/>
    Description: <input type="text" name="description"><br/>
    <input type="submit" value="Submit" />
</form>
<a href="/items">Back to list</a>